﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog_DAL
{
    /// <summary>
    /// 数据库连接工厂
    /// </summary>
    public class ConnectionFctory
    {
        private readonly static string sqlconnection = ConfigurationManager.AppSettings["DataConnection"];
        /// <summary>
        /// 获取Sql Server的连接数据库对象。
        /// </summary>
        /// <returns></returns>
        public static SqlConnection OpenConnection()
        {
            var connection = new SqlConnection(sqlconnection);  //这里sqlconnection就是数据库连接字符串
            connection.Open();
            return connection;
        }
    }
}
