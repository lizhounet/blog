﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog_DAL
{
    /// <summary>
    /// 数据库操作接口
    /// </summary>
    public interface IDbDal
    {

        T Query<T>(T t);
        List<T> QueryList<T>(T t);
        Int32 Delete<T>(T t);
        Int32 Update<T>(T t);
        Int32 Insert<T>(T t);


    }
}
