﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog_DAL
{
    public class PagingDAL : IPagingDAL
    {
        public Dictionary<string, object> getPages<T>(int PageSize, int PageIndex, string strOrder, string strTable, string strSwhere, string strColumns)
        {
            var Parameters = new DynamicParameters();
            Dictionary<string, object> dir = new Dictionary<string, object>();
            Parameters.Add("strColumns", strColumns);
            Parameters.Add("strOrder", strOrder);
            Parameters.Add("strWhere", strSwhere);
            Parameters.Add("strTable", strTable);
            Parameters.Add("PageIndex", PageIndex);
            Parameters.Add("PageSize", PageSize);
            Parameters.Add("count", 0, DbType.Int32, ParameterDirection.Output);
            using (var connection = Blog_DAL.ConnectionFctory.OpenConnection())
            {
                List<T> list = connection.Query<T>("SELECT_PAGES", Parameters, commandType: CommandType.StoredProcedure).AsList();
                int totalCount = Parameters.Get<int>("@count");//返回总页数
                dir.Add("count", totalCount);
                dir.Add("data", list);
            }
            return dir;
        }
    }
}
