﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Blog_DAL
{
    /// <summary>
    /// 博客标签表数据库操作类
    /// </summary>
    public class BlogLableDAL : IDbDal
    {
        /// <summary>
        /// 返回单个博客标签
        /// </summary>
        /// <typeparam name="T">泛型:Blog_MODEL.BlogLable</typeparam>
        /// <param name="t">泛型:Blog_MODEL.BlogLable</param>
        /// <returns></returns>
        public T Query<T>(T t)
        {
            Blog_MODEL.BlogLable Blable = t as Blog_MODEL.BlogLable;
            String strSql = "select * from BlogLable where BlogLableBh=@BlogLableBh and 1=1 ";
            using (var connection = ConnectionFctory.OpenConnection())
            {
                return connection.Query<T>(strSql, Blable).SingleOrDefault();
            }
        }
        /// <summary>
        /// 查询多个个博客标签
        /// </summary>
        /// <typeparam name="T">泛型:Blog_MODEL.BlogLable</typeparam>
        /// <param name="strWhere">查询条件</param>
        /// <returns></returns>
        public List<T> QueryList<T>(T t)
        {
            Blog_MODEL.BlogLable Blable = t as Blog_MODEL.BlogLable;
            StringBuilder strSql = new StringBuilder(20);
            var Parameters = new DynamicParameters();
            strSql.AppendLine("select * from BlogLable where 1=1");
            if (!string.IsNullOrEmpty(Blable.BlogLableName))
            {
                strSql.AppendLine(" and UPPER(BlogLableName) = @BlogLableName");
                Parameters.Add("BlogLableName", Blable.BlogLableName, DbType.String);
            }
            if (!string.IsNullOrEmpty(Blable.BlogLableNote))
            {
                strSql.AppendLine(" and BlogLableNote like @BlogLableNote");
                Parameters.Add("BlogLableNote", "%" + Blable.BlogLableNote + "%", DbType.String);
            }
            using (var connection = ConnectionFctory.OpenConnection())
            {

                return connection.Query<T>(strSql.ToString(), Parameters).ToList();
            }
        }
        /// <summary>
        /// 删除单个博客标签
        /// </summary>
        /// <typeparam name="T">泛型:Blog_MODEL.BlogLable</typeparam>
        /// <param name="t">泛型:Blog_MODEL.BlogLable</param>
        /// <returns></returns>
        public Int32 Delete<T>(T t)
        {
            Blog_MODEL.BlogLable Blable = t as Blog_MODEL.BlogLable;
            String strSql = "delete from BlogLable where BlogLableBh=@BlogLableBh ";
            using (var connection = ConnectionFctory.OpenConnection())
            {
                return connection.Execute(strSql, Blable);
            }

        }
        /// <summary>
        /// 修改单个博客标签
        /// </summary>
        /// <typeparam name="T">泛型:Blog_MODEL.BlogLable</typeparam>
        /// <param name="t">泛型:Blog_MODEL.BlogLable</param>
        /// <returns></returns>
        public Int32 Update<T>(T t)
        {
            Blog_MODEL.BlogLable Blable = t as Blog_MODEL.BlogLable;
            String strSql = "update BlogLable set BlogLableName=@BlogLableName,BlogLableSortValue=@BlogLableSortValue,BlogLableNote=@BlogLableNote where BlogLableBh=@BlogLableBh";
            using (var connection = ConnectionFctory.OpenConnection())
            {

                return connection.Execute(strSql, Blable);
            }
        }
        /// <summary>
        /// 插入管理员
        /// </summary>
        /// <typeparam name="T">泛型:Blog_MODEL.BlogLable</typeparam>
        /// <param name="t">泛型:Blog_MODEL.BlogLable</param>
        /// <returns></returns>
        public Int32 Insert<T>(T t)
        {
            Blog_MODEL.BlogLable Blable = t as Blog_MODEL.BlogLable;
            String strSql = "INSERT INTO BlogLable(BlogLableName,BlogLableSortValue,BlogLableNote) VALUES(@BlogLableName,@BlogLableSortValue,@BlogLableNote) select @@identity ";
            using (var connection = ConnectionFctory.OpenConnection())
            {
                return connection.Query<int>(strSql, Blable).First();
            }
        }
        /// <summary>
        /// 获取最大标签排序值
        /// </summary>
        /// <returns></returns>
        public Int32 getBlogLalbeSortMax()
        {
            using (var connection = Blog_DAL.ConnectionFctory.OpenConnection())
            {
                return connection.ExecuteScalar<int>("select max(BlogLableSortValue) from BlogLable");
            }
        }
    }

}
