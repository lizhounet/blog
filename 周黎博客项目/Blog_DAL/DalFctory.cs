﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blog_DAL;

namespace Blog_DAL
{
    /// <summary>
    /// 数据库操作工厂类
    /// </summary>
    public class DalFctory
    {
        /// <summary>
        /// 私有化构造函数使其不能实例化
        /// </summary>
        private DalFctory() { }
        //定义一个只读静态对象
        //且这个对象是在程序运行时创建的
        private static readonly object syncObject = new object();
        #region 创建数据库操作类
        //管理员数据库操作类
        private static AdminUsersDAL adminDal;
        //博客标签数据库操作类
        private static BlogLableDAL BLable;
        //博客文章数据库操作类
        private static BlogArticleDAL BArticle;
        //用户数据库操作类
        private static UserDAL UserDal;
        //用户数据库操作类
        private static PagingDAL PagingDAL;
        /// <summary>
        /// 创建AdminUsersDAL
        /// </summary>
        /// <returns></returns>
        public static IDbDal CreateAdminUsersDAL()
        {
            if (adminDal == null)
                lock (syncObject)//防止多线程获取实例
                    //第二重 singleton == null
                    if (adminDal == null)
                        adminDal = new AdminUsersDAL();
            return adminDal;
        }
        /// <summary>
        /// 创建BlogLableDAL
        /// </summary>
        /// <returns></returns>
        public static IDbDal CreateBlogLableDAL()
        {
            if (BLable == null)
                lock (syncObject)//防止多线程获取实例
                    //第二重 singleton == null
                    if (BLable == null)
                        BLable = new BlogLableDAL();
            return BLable;
        }
        /// <summary>
        /// 创建BlogArticleDAL
        /// </summary>
        /// <returns></returns>
        public static IDbDal CreateBlogArticleDAL()
        {
            if (BArticle == null)
                lock (syncObject)//防止多线程获取实例
                    //第二重 singleton == null
                    if (BArticle == null)
                        BArticle = new BlogArticleDAL();
            return BArticle;
        }
        /// <summary>
        /// 创建UserDal
        /// </summary>
        /// <returns></returns>
        public static IDbDal CreateUserDal()
        {
            if (UserDal == null)
                lock (syncObject)//防止多线程获取实例
                    //第二重 singleton == null
                    if (UserDal == null)
                        UserDal = new UserDAL();
            return UserDal;
        }
        /// <summary>
        /// 创建PagingDAL
        /// </summary>
        /// <returns></returns>
        public static PagingDAL CreatePagingDAL()
        {
            if (PagingDAL == null)
                lock (syncObject)//防止多线程获取实例
                    //第二重 singleton == null
                    if (PagingDAL == null)
                        PagingDAL = new PagingDAL();
            return PagingDAL;
        }
        #endregion
    }
}
