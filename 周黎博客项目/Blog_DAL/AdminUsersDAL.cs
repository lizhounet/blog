﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Blog_DAL
{
    /// <summary>
    /// 管理员数据库操作类
    /// </summary>
    public class AdminUsersDAL : IDbDal
    {
        /// <summary>
        /// 返回单个管理员
        /// </summary>
        /// <typeparam name="T">泛型:Blog_MODEL.AdminUsers</typeparam>
        /// <param name="t">泛型:Blog_MODEL.AdminUsers</param>
        /// <param name="strWhere">查询条件</param>
        /// <returns></returns>
        public T Query<T>(T t)
        {
            Blog_MODEL.AdminUsers admin = t as Blog_MODEL.AdminUsers;
            String strSql = "select * from AdminUsers where 1=1 ";
            if (admin.Id != 0)
                {
                strSql += " and id=@Id ";
            }
            if (!string.IsNullOrEmpty(admin.AdminUserName))
            {
                strSql += " and AdminUserName=@AdminUserName ";
            }
            using (var connection = ConnectionFctory.OpenConnection())
            {
                return connection.Query<T>(strSql, admin).SingleOrDefault();
            }
        }
        /// <summary>
        /// 查询多个管理员
        /// </summary>
        /// <typeparam name="T">泛型:Blog_MODEL.AdminUsers</typeparam>
        /// <param name="strWhere">查询条件</param>
        /// <returns></returns>
        public List<T> QueryList<T>(T t)
        {
            Blog_MODEL.AdminUsers admin = t as Blog_MODEL.AdminUsers;
            var Parameters = new DynamicParameters();
            StringBuilder strSql = new StringBuilder(20);
            strSql.AppendLine("select * from AdminUsers  WHERE 1=1");
            //拼接条件
            if (!string.IsNullOrEmpty(admin.AdminUserName))
            {
                strSql.AppendLine(" and AdminUserName like @AdminUserName ");
                Parameters.Add("AdminUserName", "%" + admin.AdminUserName + "%", DbType.String);
            }
            if (!string.IsNullOrEmpty(admin.AdminPhone))
            {
                strSql.AppendLine(" and AdminPhone like @AdminPhone ");
                Parameters.Add("AdminPhone", "%" + admin.AdminPhone + "%", DbType.String);
            }
            if (!string.IsNullOrEmpty(admin.AdminUsersNote))
            {
                strSql.AppendLine(" and AdminUsersNote like @AdminUsersNote ");
                Parameters.Add("AdminUsers", "%" + admin.AdminUsersNote + "%", DbType.String);
            }

            using (var connection = ConnectionFctory.OpenConnection())
            {
                return connection.Query<T>(strSql.ToString(), Parameters).ToList();
            }
        }
        /// <summary>
        /// 删除单个管理员
        /// </summary>
        /// <typeparam name="T">泛型:Blog_MODEL.AdminUsers</typeparam>
        /// <param name="t">泛型:Blog_MODEL.AdminUsers</param>
        /// <returns></returns>
        public Int32 Delete<T>(T t)
        {
            Blog_MODEL.AdminUsers admin = t as Blog_MODEL.AdminUsers;
            String strSql = "delete from AdminUsers where id=@Id ";
            using (var connection = ConnectionFctory.OpenConnection())
            {
                return connection.Execute(strSql, admin);
            }

        }
        /// <summary>
        /// 修改管理员
        /// </summary>
        /// <typeparam name="T">泛型:Blog_MODEL.AdminUsers</typeparam>
        /// <param name="t">泛型:Blog_MODEL.AdminUsers</param>
        /// <returns></returns>
        public Int32 Update<T>(T t)
        {
            Blog_MODEL.AdminUsers admin = t as Blog_MODEL.AdminUsers;
            String strSql = "update AdminUsers set AdminUserName=@AdminUserName,AdminPwd=@AdminPwd,AdminPhone=@AdminPhone,AdminUsers=@AdminUsers where Id=@Id";
            using (var connection = ConnectionFctory.OpenConnection())
            {
                return connection.Execute(strSql, admin);
            }
        }
        /// <summary>
        /// 插入管理员
        /// </summary>
        /// <typeparam name="T">泛型:Blog_MODEL.AdminUsers</typeparam>
        /// <param name="t">泛型:Blog_MODEL.AdminUsers</param>
        /// <returns></returns>
        public Int32 Insert<T>(T t)
        {
            Blog_MODEL.AdminUsers admin = t as Blog_MODEL.AdminUsers;
            String strSql = "INSERT INTO AdminUsers(AdminUserName,AdminPwd,AdminPhone,AdminUsers) VALUES(@AdminUserName,@AdminPwd,@AdminPhone,@AdminUsers) select @@identity  ";
            using (var connection = ConnectionFctory.OpenConnection())
            {
                return connection.Query<int>(strSql, admin).First();
            }
        }
    }

}
