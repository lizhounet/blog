﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog_DAL
{
   public interface IPagingDAL
    {
        Dictionary<string, object> getPages<T>(int PageSize, int PageIndex, string strOrder, string strTable, string strSwhere, string strColumns);
    }
}
