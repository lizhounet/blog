﻿using System;
using System.Collections.Generic;

namespace Blog_MODEL
{
    /// <summary>
    /// 用户表
    /// </summary>
    public  class Users
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 用户帐户
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 用户昵称
        /// </summary>
        public string UserNikeName { get; set; }
        /// <summary>
        /// 用户密码
        /// </summary>
        public string UserPwd { get; set; }
        /// <summary>
        /// 用户出生年月日
        /// </summary>
        public Nullable<System.DateTime> UserBirthday { get; set; }
        /// <summary>
        /// 用户QQ
        /// </summary>
        public string UserQQ { get; set; }
        /// <summary>
        /// 用户微信
        /// </summary>
        public string UserWX { get; set; }
        /// <summary>
        /// 用户头像
        /// </summary>
        public string UserAvatar { get; set; }
        /// <summary>
        /// 用户电话
        /// </summary>
        public string UserPhone { get; set; }
        /// <summary>
        /// 用户状态
        /// </summary>
        public int UserStatus { get; set; }
        /// <summary>
        /// 用户创建时间
        /// </summary>
        public System.DateTime UserCreateTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string UsersNote { get; set; }
    }
}
