﻿using System;
using System.Collections.Generic;

namespace Blog_MODEL
{
    /// <summary>
    /// 博客文章表
    /// </summary>
    public  class BlogArticle
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 博客标题
        /// </summary>
        public string BlogArticleTitle { get; set; }
        /// <summary>
        /// 博客内容
        /// </summary>
        public string BlogArticleBody { get; set; }
        /// <summary>
        /// 博客内容
        /// </summary>
        public string BlogArticleBody_Markdown { get; set; }
        /// <summary>
        /// 排序值
        /// </summary>
        public Int32 BlogArticleSortValue { set; get; }
        /// <summary>
        /// 博客浏览量
        /// </summary>
        public int BlogArticleBrowsingNum { get; set; }
        /// <summary>
        /// 博客点赞量
        /// </summary>
        public int BlogArticleLikeNum { get; set; }
        /// <summary>
        /// 博客评论量
        /// </summary>
        public int BlogArticleCommentNum { get; set; }
        /// <summary>
        /// 博客创建时间
        /// </summary>
        public System.DateTime? BlogArticleCreateTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string BlogArticleNote { get; set; }
        /// <summary>
        /// 博客标签
        /// </summary>
        public string BlogLableNames { get; set; }
    }
}
