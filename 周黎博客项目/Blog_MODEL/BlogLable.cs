﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog_MODEL
{
    /// <summary>
    /// 博客标签表
    /// </summary>
    public  class BlogLable
    {
        /// <summary>
        /// 博客标签编号
        /// </summary>
        public int BlogLableBh { get; set; }
        /// <summary>
        /// 博客标签名称
        /// </summary>
        public string BlogLableName { get; set; }
        /// <summary>
        /// 排序值
        /// </summary>
        public Int32 BlogLableSortValue { set; get; }
        /// <summary>
        /// 博客标签创建时间
        /// </summary>
        public System.DateTime BlogLableCreateTime { get; set; }
        /// <summary>
        /// 博客标签点击量
        /// </summary>
        public Int32 BlogLableClicks { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string BlogLableNote { get; set; }
    }
}
