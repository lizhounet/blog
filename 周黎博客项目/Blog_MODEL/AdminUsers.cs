﻿using System;
using System.Collections.Generic;

namespace Blog_MODEL
{

    /// <summary>
    /// 管理员表
    /// </summary>
    public class AdminUsers
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 管理员帐户
        /// </summary>
        public string AdminUserName { get; set; }
        /// <summary>
        /// 管理员密码
        /// </summary>
        public string AdminPwd { get; set; }
        /// <summary>
        /// 管理员手机
        /// </summary>
        public string AdminPhone { get; set; }
        /// <summary>
        /// 帐户创建时间
        /// </summary>
        public System.DateTime AdminCreateTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string AdminUsersNote { get; set; }
    }
}
