﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog_MODEL
{
    /// <summary>
    /// 标签关联表
    /// </summary>
    public  class BlogRelated
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 博客文章Id
        /// </summary>
        public int BlogRelatedArticleId { get; set; }
        /// <summary>
        /// 博客标签编号
        /// </summary>
        public string BlogRelateLableId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTime BlogRelatedCreateTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string BlogRelatedNote { get; set; }
    }
}
