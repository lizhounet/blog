﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace Blog_BLL
{
    /// <summary>
    /// 博客文章数据库逻辑操作类
    /// </summary>
    public class BlogArticleBLL
    {
        private readonly static Blog_DAL.IDbDal _BlogArticleDAL = Blog_DAL.DalFctory.CreateBlogArticleDAL();
        /// <summary>
        /// 查询单个博客文章
        /// </summary>
        /// <param name="BlogArticle"></param>
        /// <returns></returns>
        public static Blog_MODEL.BlogArticle Query(Blog_MODEL.BlogArticle BlogArticle)
        {
            return _BlogArticleDAL.Query<Blog_MODEL.BlogArticle>(BlogArticle);
        }
        /// <summary>
        /// 查询多个博客文章
        /// </summary>
        /// <param name="strWhere">查询条件</param>
        /// <returns></returns>
        public static List<Blog_MODEL.BlogArticle> QueryList(Blog_MODEL.BlogArticle BlogArticle)
        {
            return _BlogArticleDAL.QueryList<Blog_MODEL.BlogArticle>(BlogArticle);
        }
        /// <summary>
        /// 新增单个博客文章
        /// </summary>
        /// <param name="BlogArticle"></param>
        /// <returns></returns>
        public static Int32 Insert(Blog_MODEL.BlogArticle BlogArticle)
        {
            return _BlogArticleDAL.Insert(BlogArticle);
        }
        /// <summary>
        /// 删除单个博客文章
        /// </summary>
        /// <param name="BlogArticle"></param>
        /// <returns></returns>
        public static Int32 Delete(Blog_MODEL.BlogArticle BlogArticle)
        {
            return _BlogArticleDAL.Delete(BlogArticle);
        }
        /// <summary>
        /// 修改单个博客文章
        /// </summary>
        /// <param name="BlogArticle"></param>
        /// <returns></returns>
        public static Int32 Update(Blog_MODEL.BlogArticle BlogArticle)
        {
            return _BlogArticleDAL.Update(BlogArticle);
        }
        /// <summary>
        /// 新增标签关联
        /// </summary>
        /// <param name="BlogArticle"></param>
        /// <returns></returns>
        public static Int32 InsertBlogRelated(Blog_MODEL.BlogRelated BlogRelated)
        {
            return (_BlogArticleDAL as Blog_DAL.BlogArticleDAL).InsertBlogRelated(BlogRelated);
        }
        /// <summary>
        /// 获取文章条数
        /// </summary>
        /// <param name="BlogArticle"></param>
        /// <returns></returns>
        public static Int32 getBlogArticleCount(Blog_MODEL.BlogArticle BlogArticle)
        {
            return (_BlogArticleDAL as Blog_DAL.BlogArticleDAL).getBlogArticleCount(BlogArticle);
        }
        /// <summary>
        /// 获取最大文章排序值
        /// </summary>
        /// <returns></returns>
        public static Int32 getBlogSortMax()
        {
            return (_BlogArticleDAL as Blog_DAL.BlogArticleDAL).getBlogSortMax();
        }
        /// <summary>
        /// 添加博客文章
        /// </summary>
        /// <returns></returns>
        public static Int32 InsertBlogArticleTransaction(List<Blog_MODEL.BlogLable> listBlogLable, Blog_MODEL.BlogArticle _BlogArticle)
        {
            return (_BlogArticleDAL as Blog_DAL.BlogArticleDAL).InsertBlogArticleTransaction(listBlogLable, _BlogArticle);
        }
        /// <summary>
        /// 添加博客文章
        /// </summary>
        /// <returns></returns>
        public static Int32 UpdateBlogArticleTransaction(List<Blog_MODEL.BlogLable> listBlogLable, Blog_MODEL.BlogArticle _BlogArticle)
        {
            return (_BlogArticleDAL as Blog_DAL.BlogArticleDAL).UpdateBlogArticleTransaction(listBlogLable, _BlogArticle);
        }
        /// <summary>
        /// 查询博客标签
        /// </summary>
        /// <param name="BlogRelatedArticleId">博客文章Id</param>
        /// <returns></returns>
        public static List<Blog_MODEL.BlogLable> QueryBlogLable(int BlogRelatedArticleId)
        {
            return (_BlogArticleDAL as Blog_DAL.BlogArticleDAL).QueryBlogLable(BlogRelatedArticleId);
        }
        /// <summary>
        /// 获取置顶博客
        /// </summary>
        /// <returns></returns>
        public static Blog_MODEL.BlogArticle getObenBlogArticle()
        {
            return (_BlogArticleDAL as Blog_DAL.BlogArticleDAL).getObenBlogArticle();
        }
        /// <summary>
        /// 博客详情
        /// </summary>
        /// <returns></returns>
        public static List<Blog_MODEL.BlogArticle> getBlogArticleShow(int Id)
        {
            return (_BlogArticleDAL as Blog_DAL.BlogArticleDAL).getBlogArticleShow(Id);
        }
        public static int addBrowsingNum(Blog_MODEL.BlogArticle BlogArticle)
        {
            return (_BlogArticleDAL as Blog_DAL.BlogArticleDAL).addBrowsingNum(BlogArticle);
        }
    }
}
