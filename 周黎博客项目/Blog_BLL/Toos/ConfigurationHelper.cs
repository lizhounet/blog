﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Blog_BLL.Toos
{
    /// <summary>
    /// 配置文件帮助类
    /// </summary>
    public static class ConfigurationHelper
    {
        #region GetConnectionStrings
        /// <summary>
        /// 取AppSetting结点数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string getAppSetting(string key)
        {
            if (ConfigurationManager.AppSettings[key] != null)
                return ConfigurationManager.AppSettings[key].ToString();
            return null;
        }
        #endregion
        #region AddAppSetting
        /// <summary>
        /// 添加AppSetting结点数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Boolean setAppSetting(string key, string value)
        {
            if (ConfigurationManager.AppSettings[key] != null)
                System.Configuration.ConfigurationManager.AppSettings.Set(key, value);
            else
                System.Configuration.ConfigurationManager.AppSettings.Add(key, value);
            return true;
        }
        #endregion
        #region GetConnectionStrings
        /// <summary>
        /// 取connectionStrings结点数据
        /// </summary>
        /// <param name="key">name</param>
        /// <returns>返回值</returns>
        public static string GetConnectionStrings(string key)
        {
            if (ConfigurationManager.ConnectionStrings[key] != null)
                return ConfigurationManager.ConnectionStrings[key].ToString();
            return null;
        }
        #endregion
        #region AddConnectionStrings
        /// <summary>
        /// 修改AppSetting结点数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Boolean setConnectionStrings(string key, string value)
        {
            System.Configuration.ConfigurationManager.ConnectionStrings[key].ConnectionString = value;
            return true;
        }
        #endregion
     
    }
}
