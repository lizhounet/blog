﻿using Blog_MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog_BLL
{
    /// <summary>
    /// 用户登录操作类
    /// </summary>
    public class UserLoginBLL
    {
        private readonly static string UserInfoFreeLoginCookie = Blog_BLL.Toos.ConfigurationHelper.getAppSetting("UserInfoFreeLoginCookie");//保存Cookie名称
        private readonly static string UserInfoFreeLoginSession = Blog_BLL.Toos.ConfigurationHelper.getAppSetting("UserInfoFreeLoginSession");//保存Session名称
        /// <summary>
        /// 登录系统方法
        /// </summary>
        /// <param name="UserName">账号</param>
        /// <param name="UserPwd">密码</param>
        /// <param name="isRememberPwd">是否记住密码</param>
        /// <param name="LoginType">登录类型(0:用户登录,1:管理员登录)默认用户登录</param>
        /// <returns></returns>
        public static JsonMessages UserLogin(string UserName, string UserPwd, int LoginType = 0)
        {
            JsonMessages isLogin = null;
            if (LoginType == 1)
                isLogin = AdminAccountLogin(UserName, UserPwd);
            else
                isLogin = UserAccountLogin(UserName, UserPwd);
            return isLogin;
        }
        /// <summary>
        /// 管理员登录
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="UserPwd"></param>
        /// <param name="isRememberPwd"></param>
        /// <returns></returns>
        private static JsonMessages AdminAccountLogin(string UserName, string UserPwd)
        {
            JsonMessages jsonMess = new JsonMessages();
            AdminUsers user = Blog_BLL.AdminUsersBLL.Query(new AdminUsers
            {
                AdminUserName = UserName,
            });
            if (user == null)
            {
                jsonMess.StateCode = StateCode.failure;
                jsonMess.Messages = "没有注册该账号";
            }
            else
            {
                if (user.AdminPwd.Equals(UserPwd.GetMd5Hash()))
                {
                    string strUserInfoFreeLogin = (UserName + "," + UserPwd + ",1").Base64Encode(Encoding.UTF8);
                    if (Blog_BLL.CookiesBLL.Add(UserInfoFreeLoginCookie, strUserInfoFreeLogin))
                    {
                        if (Blog_BLL.SessionBLL.Add(UserInfoFreeLoginSession, user))
                        {
                            jsonMess.StateCode = StateCode.success;
                            jsonMess.Messages = "登陆成功";
                        }
                        else
                        {
                            jsonMess.StateCode = StateCode.failure;
                            jsonMess.Messages = "记录登陆状态时出错(session)";
                        }

                    }
                    else
                    {
                        jsonMess.StateCode = StateCode.failure;
                        jsonMess.Messages = "记录登陆状态时出错(Cookies)";
                    }
                }
                else
                {
                    jsonMess.StateCode = StateCode.failure;
                    jsonMess.Messages = "密码错误";
                }
            }

            return jsonMess;
        }
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="UserPwd"></param>
        /// <param name="isRememberPwd"></param>
        /// <returns></returns>
        private static JsonMessages UserAccountLogin(string UserName, string UserPwd)
        {
            JsonMessages jsonMess = new JsonMessages();
            Users user = Blog_BLL.UsersBLL.Query(new Users
            {
                UserName = UserName,
            });
            if (user == null)
            {
                jsonMess.StateCode = StateCode.failure;
                jsonMess.Messages = "没有注册该账号";
            }
            else
            {
                if (user.UserPwd.Equals(UserPwd.GetMd5Hash()))
                {
                    string strUserInfoFreeLogin = (UserName + "," + UserPwd + ",0").Base64Encode(Encoding.UTF8);
                    if (Blog_BLL.CookiesBLL.Add(UserInfoFreeLoginCookie, strUserInfoFreeLogin))
                    {
                        if (Blog_BLL.SessionBLL.Add(UserInfoFreeLoginSession, user))
                        {
                            jsonMess.StateCode = StateCode.success;
                            jsonMess.Messages = "登陆成功";
                        }
                        else
                        {
                            jsonMess.StateCode = StateCode.failure;
                            jsonMess.Messages = "记录登陆状态时出错(session)";
                        }

                    }
                    else
                    {
                        jsonMess.StateCode = StateCode.failure;
                        jsonMess.Messages = "记录登陆状态时出错(Cookies)";
                    }
                }
                else
                {
                    jsonMess.StateCode = StateCode.failure;
                    jsonMess.Messages = "密码错误";
                }
            }

            return jsonMess;
        }
        /// <summary>
        /// 登录验证
        /// </summary>
        /// <returns></returns>
        public static JsonMessages CheckLogin()
        {
            JsonMessages jsonMess = new JsonMessages();
            if (System.Web.HttpContext.Current.Request.Cookies[UserInfoFreeLoginCookie] != null)
            {
                string[] _UserInfoFreeLogin = System.Web.HttpContext.Current.Request.Cookies[UserInfoFreeLoginCookie].Value.Base64Decode(Encoding.UTF8).Split(',');
                if (Convert.ToInt32(_UserInfoFreeLogin[2]) == 1)//管理员
                {
                    AdminUsers user = Blog_BLL.AdminUsersBLL.Query(new AdminUsers
                    {
                        AdminUserName = _UserInfoFreeLogin[0],
                    });
                    if (user == null)
                    {
                        jsonMess.StateCode = StateCode.failure;
                        jsonMess.Messages = "账户已失效";
                    }
                    else
                    {
                        if (user.AdminPwd.Equals(_UserInfoFreeLogin[1].GetMd5Hash()))
                        {
                            if (Blog_BLL.SessionBLL.Get(UserInfoFreeLoginSession) == null)
                                Blog_BLL.SessionBLL.Add(UserInfoFreeLoginSession, user);
                            jsonMess.StateCode = StateCode.success;
                            jsonMess.Messages = "登陆正常";
                        }
                        else
                        {
                            jsonMess.StateCode = StateCode.failure;
                            jsonMess.Messages = "密码检测到已更改,请重新登录";
                        }
                    }
                }
                if (Convert.ToInt32(_UserInfoFreeLogin[2]) == 2)
                {
                    Users user = Blog_BLL.UsersBLL.Query(new Users
                    {
                        UserName = _UserInfoFreeLogin[0],
                    });
                    if (user == null)
                    {
                        jsonMess.StateCode = StateCode.failure;
                        jsonMess.Messages = "账户已失效";
                    }
                    else
                    {
                        if (user.UserPwd.Equals(_UserInfoFreeLogin[1].GetMd5Hash()))
                        {
                            if (Blog_BLL.SessionBLL.Get(UserInfoFreeLoginSession) == null)
                                Blog_BLL.SessionBLL.Add(UserInfoFreeLoginSession, user);
                            jsonMess.StateCode = StateCode.success;
                            jsonMess.Messages = "登陆正常";
                        }
                        else
                        {
                            jsonMess.StateCode = StateCode.failure;
                            jsonMess.Messages = "密码检测到已更改,请重新登录";
                        }
                    }
                }
            }
            else
            {
                jsonMess.StateCode = StateCode.failure;
                jsonMess.Messages = "密码已失效,请重新登录";
            }
            return jsonMess;
        }

        /// <summary>
        /// 退出
        /// </summary>
        /// <returns></returns>
        public static bool LoginOut()
        {
            if (System.Web.HttpContext.Current.Request.Cookies[UserInfoFreeLoginCookie] != null)
            {
                System.Web.HttpContext.Current.Response.Cookies[UserInfoFreeLoginCookie].Expires = DateTime.Now;
            }
            Blog_BLL.SessionBLL.Remove(UserInfoFreeLoginSession);
            return true;
        }

    }
}
