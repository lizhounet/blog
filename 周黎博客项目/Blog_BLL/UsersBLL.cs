﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog_BLL
{
    /// <summary>
    /// 管理员数据库逻辑操作类
    /// </summary>
    public class AdminUsersBLL
    {
        private readonly static Blog_DAL.IDbDal _IDbDal = Blog_DAL.DalFctory.CreateAdminUsersDAL();
        /// <summary>
        /// 查询单个管理员
        /// </summary>
        /// <param name="AdminUsers"></param>
        /// <returns></returns>
        public static Blog_MODEL.AdminUsers Query(Blog_MODEL.AdminUsers _AdminUsers)
        {
            return _IDbDal.Query<Blog_MODEL.AdminUsers>(_AdminUsers);
        }
        /// <summary>
        /// 查询多个管理员
        /// </summary>
        /// <param name="strWhere">查询条件</param>
        /// <returns></returns>
        public static List<Blog_MODEL.AdminUsers> QueryList(Blog_MODEL.AdminUsers _AdminUsers)
        {
            return _IDbDal.QueryList<Blog_MODEL.AdminUsers>(_AdminUsers);
        }
        /// <summary>
        /// 新增单个管理员
        /// </summary>
        /// <param name="AdminUsers"></param>
        /// <returns></returns>
        public static Int32 Insert(Blog_MODEL.AdminUsers _AdminUsers)
        {
            return _IDbDal.Insert(_AdminUsers);
        }
        /// <summary>
        /// 删除单个管理员
        /// </summary>
        /// <param name="AdminUsers"></param>
        /// <returns></returns>
        public static Int32 Delete(Blog_MODEL.AdminUsers _AdminUsers)
        {
            return _IDbDal.Delete(_AdminUsers);
        }
        /// <summary>
        /// 修改单个管理员
        /// </summary>
        /// <param name="AdminUsers"></param>
        /// <returns></returns>
        public static Int32 Update(Blog_MODEL.AdminUsers _AdminUsers)
        {
            return _IDbDal.Update(_AdminUsers);
        }
    }
}
