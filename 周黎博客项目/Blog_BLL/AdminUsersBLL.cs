﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog_BLL
{
    /// <summary>
    /// 用户数据库逻辑操作类
    /// </summary>
    public class UsersBLL
    {
        private readonly static Blog_DAL.IDbDal _IDbDal = Blog_DAL.DalFctory.CreateUserDal();
        /// <summary>
        /// 查询单个用户
        /// </summary>
        /// <param name="Users"></param>
        /// <returns></returns>
        public static Blog_MODEL.Users Query(Blog_MODEL.Users _Users)
        {
            return _IDbDal.Query<Blog_MODEL.Users>(_Users);
        }
        /// <summary>
        /// 查询多个用户
        /// </summary>
        /// <param name="strWhere">查询条件</param>
        /// <returns></returns>
        public static List<Blog_MODEL.Users> QueryList(Blog_MODEL.Users _Users)
        {
            return _IDbDal.QueryList<Blog_MODEL.Users>(_Users);
        }
        /// <summary>
        /// 新增单个用户
        /// </summary>
        /// <param name="Users"></param>
        /// <returns></returns>
        public static Int32 Insert(Blog_MODEL.Users _Users)
        {
            return _IDbDal.Insert(_Users);
        }
        /// <summary>
        /// 删除单个用户
        /// </summary>
        /// <param name="Users"></param>
        /// <returns></returns>
        public static Int32 Delete(Blog_MODEL.Users _Users)
        {
            return _IDbDal.Delete(_Users);
        }
        /// <summary>
        /// 修改单个用户
        /// </summary>
        /// <param name="Users"></param>
        /// <returns></returns>
        public static Int32 Update(Blog_MODEL.Users _Users)
        {
            return _IDbDal.Update(_Users);
        }
    }
}
