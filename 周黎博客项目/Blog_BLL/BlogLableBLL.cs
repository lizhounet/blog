﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog_BLL
{
    /// <summary>
    /// 博客标签数据库逻辑操作类
    /// </summary>
    public class BlogLableBLL
    {
        private readonly static Blog_DAL.IDbDal _BlogLableDAL = Blog_DAL.DalFctory.CreateBlogLableDAL();
        /// <summary>
        /// 查询单个博客标签
        /// </summary>
        /// <param name="BlogLable"></param>
        /// <returns></returns>
        public static Blog_MODEL.BlogLable Query(Blog_MODEL.BlogLable _BlogLable)
        {
            return _BlogLableDAL.Query<Blog_MODEL.BlogLable>(_BlogLable);
        }
        /// <summary>
        /// 查询单个博客标签
        /// </summary>
        /// <param name="strWhere">查询条件</param>
        /// <returns></returns>
        public static List<Blog_MODEL.BlogLable> QueryList(Blog_MODEL.BlogLable _BlogLable)
        {
            return _BlogLableDAL.QueryList<Blog_MODEL.BlogLable>(_BlogLable);
        }
        /// <summary>
        /// 新增单个博客标签
        /// </summary>
        /// <param name="BlogLable"></param>
        /// <returns></returns>
        public static Int32 Insert(Blog_MODEL.BlogLable BlogLable)
        {
            return _BlogLableDAL.Insert(BlogLable);
        }
        /// <summary>
        /// 删除单个博客标签
        /// </summary>
        /// <param name="BlogLable"></param>
        /// <returns></returns>
        public static Int32 Delete(Blog_MODEL.BlogLable BlogLable)
        {
            return _BlogLableDAL.Delete(BlogLable);
        }
        /// <summary>
        /// 修改单个博客标签
        /// </summary>
        /// <param name="BlogLable"></param>
        /// <returns></returns>
        public static Int32 Update(Blog_MODEL.BlogLable BlogLable)
        {
            return _BlogLableDAL.Update(BlogLable);
        }
        /// <summary>
        /// 获取最大标签排序值
        /// </summary>
        /// <returns></returns>
        public static Int32 getBlogLalbeSortMax()
        {
            using (var connection = Blog_DAL.ConnectionFctory.OpenConnection())
            {
                return (_BlogLableDAL as Blog_DAL.BlogLableDAL).getBlogLalbeSortMax();
            }
        }
    }
}
