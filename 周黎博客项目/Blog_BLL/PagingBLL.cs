﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace Blog_BLL
{
    /// <summary>
    /// 通用分页获取数据类
    /// </summary>
    public class PagingBLL
    {
        private readonly static Blog_DAL.IPagingDAL _IPagingDAL = Blog_DAL.DalFctory.CreatePagingDAL();
        /// <summary>
        /// 通用分页获取数据方法
        /// </summary>
        /// <param name="PageSize">每页数量</param>
        /// <param name="PageIndex">需要获取第几页数据</param>
        /// <param name="strOrder">排序条件</param>
        /// <param name="strTable">查询的表名</param>
        /// <param name="strSwhere">查询的条件</param>
        /// <returns></returns>
        public static Dictionary<string, object> getPages<T>(int PageSize, int PageIndex, string strOrder, string strTable, string strSwhere = " 1=1 ", string strColumns=" * ")
        {
            return _IPagingDAL.getPages<T>(PageSize, PageIndex, strOrder, strTable, strSwhere, strColumns);
        }
    }
}
