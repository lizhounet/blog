-------------------------------------------------------------------博客涉及到表的创建语句开始
CREATE DATABASE zl_blog --创建数据库
--管理员表
CREATE TABLE AdminUsers(
ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,--主键
AdminUserName NVARCHAR(20) NOT NULL UNIQUE,--管理员账号
AdminPwd NVARCHAR(50) DEFAULT substring(sys.fn_sqlvarbasetostr(HashBytes('MD5','123456')),3,32) NOT NULL,--管理员密码
AdminPhone VARCHAR(11) NULL,--管理员手机
UserStatus INT  DEFAULT 1 NOT NULL,--用户状态
AdminCreateTime DATETIME  DEFAULT CONVERT(varchar, getdate(), 120 )  NOT NULL,--创建时间
AdminUsersNote NVARCHAR(2048) NULL--备注
)
--用户表
CREATE TABLE Users(
ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL ,--主键
UserName NVARCHAR(20) NOT NULL UNIQUE,--用户帐号
UserNikeName NVARCHAR(20) NOT NULL,--用户昵称
UserPwd NVARCHAR(50) DEFAULT substring(sys.fn_sqlvarbasetostr(HashBytes('MD5','123456')),3,32) NOT NULL,--用户密码
UserBirthday DATE  NULL,--用户出生年月日
UserQQ VARCHAR(15)  NULL,--用户QQ
UserWX VARCHAR(30)  NULL,--用户微信
UserAvatar VARCHAR(150)  NULL,--用户头像
UserPhone VARCHAR(11) NULL,--用户手机
UserStatus INT  DEFAULT 1 NOT NULL,--用户状态
UserCreateTime DATETIME  DEFAULT CONVERT(varchar, getdate(), 120 )  NOT NULL,--创建时间
UserNote NVARCHAR(2048) NULL--备注
)
--博客标签表
CREATE TABLE BlogLable(
BlogLableBh INT IDENTITY(1,1) PRIMARY KEY NOT NULL,--博客标签编号(主键)
BlogLableName NVARCHAR(20) NOT NULL UNIQUE,--博客标签名称
BlogLableSortValue INT DEFAULT 0 NOT NULL,--排序值,
BlogLableClicks INT DEFAULT 0 NOT NULL,--博客标签点击量
BlogLableCreateTime DATETIME  DEFAULT CONVERT(varchar, getdate(), 120 ) NOT NULL,--创建时间
BlogLableNote NVARCHAR(2048) NULL--备注
)
--博客文章表
CREATE TABLE BlogArticle(
Id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,--主键
BlogArticleTitle NVARCHAR(50) NOT NULL,--博客文章标题
BlogArticleBody NTEXT NOT NULL,--博客文章内容
BlogArticleBody_Markdown NTEXT  NULL,--博客文章内容,
BlogArticleSortValue INT DEFAULT 0 NOT NULL,--排序值,
BlogArticleBrowsingNum INT DEFAULT 0 NOT NULL,--博客文章浏览量
BlogArticleLikeNum INT DEFAULT 0 NOT NULL,--博客文章点赞量
BlogArticleCommentNum INT DEFAULT 0 NOT NULL,--博客文章评论量
BlogArticleCreateTime DATETIME  DEFAULT CONVERT(varchar, getdate(), 120 ) NOT NULL,--创建时间
BlogArticleNote NVARCHAR(2048) NULL--备注
)

--标签关联表
CREATE TABLE BlogRelated(
ID INT IDENTITY(1,1) PRIMARY KEY NOT NULL,--主键
BlogRelatedArticleId INT  NOT NULL,--博客文章Id
BlogRelateLableId INT NOT NULL,--博客标签编号
BlogRelatedCreateTime DATETIME  DEFAULT CONVERT(varchar, getdate(), 120 ) NOT NULL,--创建时间
BlogRelatedNote NVARCHAR(2048) NULL--备注
)
-------------------------------------------------------------------博客涉及到表的创建语句结束

-------------------------------------------------------------------博客分页查询通用存储过程开始

/*说明:通用分页存储过程*/
/*注意:where条件里面使用到的列必须在@strColumns参数里面包含*/
Create PROC SELECT_PAGES
	@strColumns varchar(max), /*字段名*/
	@strOrder varchar(max), /*排序条件*/
	@strWhere varchar(max), /*条件*/
	@strTable varchar(20), /*表名*/
	@PageIndex int, /*第几页*/
    @PageSize int, /*每页数量*/
    @count int output /*总条数*/
	AS
	declare 
	@BEGIN INT,
    @END INT,
    @sqlcount nvarchar(max)
	BEGIN
	set nocount on;
	SET @BEGIN=@PageSize * (@PageIndex - 1) + 1
	SET @END=@PageSize * @PageIndex
	    /*开始分页查询*/
		exec('SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY '+@strOrder+') RN,* FROM (SELECT '+@strColumns+' FROM '+@strTable+' ) A where '+@strWhere+') T
		WHERE RN BETWEEN '+ @BEGIN+ ' AND ' +@END)
		/*获取总条数*/
		set @sqlcount='SELECT @ct=count(1) FROM (SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY '+@strOrder+') RN,'+@strColumns+' FROM '+@strTable+' ) A where '+@strWhere+') T'		
		exec sp_executesql @sqlcount,N'@ct int output',@count output 
		set nocount off;
	END

-------------------------------------------------------------------博客分页查询通用存储过程结束
