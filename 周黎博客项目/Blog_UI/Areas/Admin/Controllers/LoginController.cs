﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_MODEL;
using Blog_BLL;

namespace Blog_UI.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        // GET: Admin/Login
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 管理员登录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string login()
        {
            String strUserName = Request.Form["UserName"] == null ? "" : Request.Form["UserName"];//用户帐号
            String strUserPwd = Request.Form["UserPwd"] == null ? "" : Request.Form["UserPwd"];//用户密码
            JsonMessages jsonMeg = UserLoginBLL.UserLogin(strUserName, strUserPwd, 1);//1为管理员登录
            return JsonHelper.ToJsonData(jsonMeg);


        }
        /// <summary>
        /// 注销方法
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            UserLoginBLL.LoginOut();
            return View("Index");
        }
    }

}