﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog_UI.Areas.Admin.Controllers
{
    public class HomeController : Blog_UI.Controllers.DefaultController
    {
        /// <summary>
        /// 后台主页
        /// </summary>
        /// <returns></returns>
        public ActionResult Backstage_home()
        {
            ViewBag.UserNikeName = ((Blog_MODEL.AdminUsers)Blog_BLL.SessionBLL.Get(Blog_BLL.Toos.ConfigurationHelper.getAppSetting("UserInfoFreeLoginSession"))).AdminUserName.ToString();
            return View();
        }
       /// <summary>
       /// 默认首页内容
       /// </summary>
       /// <returns></returns>
        public ActionResult Main()
        {

            return View();
        }
    }

}