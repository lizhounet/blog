﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_BLL;
using Blog_MODEL;

namespace Blog_UI.Areas.Admin.Controllers
{
    public class BlogController : Blog_UI.Controllers.DefaultController
    {
        // GET: Admin/Blog
        public ActionResult Article()
        {
            return View();
        }
        #region 分页获取博客文章
        /// <summary>
        /// 分页获取博客文章
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public String articlepage()
        {
            //标题
            string blogTitle = Request.QueryString["blogtitle"] == null ? "" : Request.QueryString["blogtitle"].Filter(); ;
            //当前第几页
            int strpage = Request.QueryString["page"] == null ? 1 : Convert.ToInt32(Request.QueryString["page"].Filter());
            //每页多少条
            int strlimit = Request.QueryString["limit"] == null ? 1 : Convert.ToInt32(Request.QueryString["limit"].Filter());
            string strwhere = " 1=1 and ";
            if (!string.IsNullOrEmpty(blogTitle))
                strwhere += " upper(BlogArticleTitle) like '%" + blogTitle.ToUpper() + "%'";
            else
                strwhere += " 1=1";
            //分页查询数据
            Dictionary<string, object> dir = Blog_BLL.PagingBLL.getPages<Blog_MODEL.BlogArticle>(strlimit, strpage, "BlogArticleSortValue desc,BlogArticleCreateTime desc", "BlogArticle", strwhere);
            string studentsJson = JsonHelper.ToJsonData(dir["data"]);
            string json = "{\"code\":0,\"msg\":\"ok\",\"count\":" + dir["count"].ToString() + ",\"data\":" + studentsJson + "}";
            return json;
        }
        #endregion
        #region 获取博客标签
        /// <summary>
        /// 获取博客标签
        /// </summary>
        /// <returns></returns>
        public String getbolglable()
        {
            return JsonHelper.ToJsonData(Blog_BLL.BlogLableBLL.QueryList(new BlogLable { }));
        }
        #endregion
        #region 获取博客属于的标签
        /// <summary>
        /// 获取博客属于的标签
        /// </summary>
        /// <param name="BlogRelatedArticleId"></param>
        /// <returns></returns>
        public string getBlogRelated(int BlogRelatedArticleId)
        {
            List<BlogLable> strBlogRelated = BlogArticleBLL.QueryBlogLable(BlogRelatedArticleId);

            return JsonHelper.ResponseData(StateCode.success, "获取成功", strBlogRelated);
        }
        #endregion
        #region 添加修改博客文章
        /// <summary>
        /// 添加修改博客文章
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]
        public string addblogartcle()
        {
            //添加修改表示(0添加1修改)
            int state = Request.Form["state"] == null ? 0 : Convert.ToInt32(Request.Form["state"]);
            //博客标题
            string BlogArticleTitle = Request.Form["BlogArticleTitle"] == null ? "" : Request.Form["BlogArticleTitle"];
            //博客标签
            string BlogLableBh = Request.Form["BlogLableBh"] == null ? "" : Request.Form["BlogLableBh"];
            //博客排序值
            string BlogArticleSortValue = Request.Form["BlogArticleSortValue"] == null ? "" : Request.Form["BlogArticleSortValue"];
            //编辑器内容
            string edits = Request.Form["edits"] == null ? "" : Request.Form["edits"];
            //备注
            string BlogArticleNote = Request.Form["BlogArticleNote"] == null ? "" : Request.Form["BlogArticleNote"];
            //博客Id
            string Id = string.IsNullOrEmpty(Request.Form["Id"]) ? "0" : Request.Form["Id"];
            string[] BlogLableBhs = BlogLableBh.Split(',');
            int intResult = 0;
            if (state == 0)
            {
                List<Blog_MODEL.BlogArticle> listBlogArticle = Blog_BLL.BlogArticleBLL.QueryList(new Blog_MODEL.BlogArticle { BlogArticleTitle = BlogArticleTitle.ToUpper() });
                if (listBlogArticle.Count > 0)
                    return JsonHelper.ResponseData(StateCode.failure, "操作失败,已经有此标题的博客", "[]");
            }
            List<BlogLable> listBlogLable = new List<BlogLable>();
            foreach (var v in BlogLableBhs)
            {
                listBlogLable.Add(new BlogLable
                {
                    BlogLableBh = Convert.ToInt32(v)
                });
            }
            string[] imglist = edits.GetHtmlImageUrlList();
            foreach (var item in imglist)
            {
                edits = edits.Replace(item, item.CrackFdl());
            }
            BlogArticle _BlogArticle = new BlogArticle
            {
                BlogArticleTitle = BlogArticleTitle,
                BlogArticleSortValue = Convert.ToInt32(BlogArticleSortValue),
                BlogArticleBody = edits,
                BlogArticleNote = BlogArticleNote,
                Id = Convert.ToInt32(Id)
            };

            if (state == 0)//添加
            {
                intResult = BlogArticleBLL.InsertBlogArticleTransaction(listBlogLable, _BlogArticle);
            }
            else
            {//修改
                intResult = BlogArticleBLL.UpdateBlogArticleTransaction(listBlogLable, _BlogArticle);
            }
            string jsonData = null;
            if (intResult > 0)
                jsonData = JsonHelper.ResponseData(StateCode.success, "操作成功", "[]");
            else
                jsonData = JsonHelper.ResponseData(StateCode.failure, "操作失败", "[]");
            return jsonData;
        }
        #endregion
        #region 删除博客文章
        /// <summary>
        /// 删除博客文章
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string blogdel()
        {
            Int32 Id = Request.Form["id"] == null ? 0 : Convert.ToInt32(Request.Form["id"]);
            Int32 intResult = BlogArticleBLL.Delete(new BlogArticle { Id = Id });
            string jsonData = null;
            if (intResult > 0)
                jsonData = JsonHelper.ResponseData(StateCode.success, "删除成功", "[]");
            else
                jsonData = JsonHelper.ResponseData(StateCode.failure, "删除失败", "[]");
            return jsonData;
        }
        #endregion
        #region 获取博客文章最大排序值
        /// <summary>
        /// 获取博客文章最大排序值
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Int32 bolgsortmax()
        {
            return BlogArticleBLL.getBlogSortMax();
        }
        #endregion
        #region 上传博客内容图片
        /// <summary>
        /// 上传博客内容图片
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string uploadimg()
        {
            String Path = @"/Upload/Images/BlogContent/";
            String strMapPath = Server.MapPath(Path);
            string fileName = Blog_BLL.Upload.UploadFile(Request, Blog_BLL.UploadFileType.ImageType, strMapPath);
            string strResult = null;
            if (!String.IsNullOrEmpty(fileName))
                strResult = "{\"code\": 0,\"msg\": \"上传成功\",\"data\":{\"src\":\"" + Path + fileName + "\",\"title\":\"\"}}";
            else
                strResult = "{\"code\": 1,\"msg\": \"上传失败\",\"data\":{}}";
            return JsonHelper.ToJsonData(strResult);
        }
        #endregion
    }
}