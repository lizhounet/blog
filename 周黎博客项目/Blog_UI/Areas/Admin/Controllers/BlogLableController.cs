﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_BLL;
using Blog_MODEL;

namespace Blog_UI.Areas.Admin.Controllers
{
    public class BlogLableController : Controller
    {
        // GET: Admin/BlogLable
        public ActionResult Lable()
        {
            return View();
        }
        #region 分页获取博客标签
        /// <summary>
        /// 分页获取博客标签
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public String lablepage()
        {
            //标题
            string soulable = Request.QueryString["soulable"] == null ? "" : Request.QueryString["soulable"].Filter(); ;
            //当前第几页
            int strpage = Request.QueryString["page"] == null ? 1 : Convert.ToInt32(Request.QueryString["page"].Filter());
            //每页多少条
            int strlimit = Request.QueryString["limit"] == null ? 1 : Convert.ToInt32(Request.QueryString["limit"].Filter());
            string strwhere = " 1=1 and ";
            if (!string.IsNullOrEmpty(soulable))
                strwhere += " upper(BlogLableName) like '%" + soulable.ToUpper() + "%'";
            else
                strwhere += " 1=1";
            //分页查询数据
            Dictionary<string, object> dir = Blog_BLL.PagingBLL.getPages<Blog_MODEL.BlogLable>(strlimit, strpage, "BlogLableSortValue desc,BlogLableCreateTime desc", "BlogLable", strwhere);
            string studentsJson = JsonHelper.ToJsonData(dir["data"]);
            string json = "{\"code\":0,\"msg\":\"ok\",\"count\":" + dir["count"].ToString() + ",\"data\":" + studentsJson + "}";
            return json;
        }
        #endregion
        #region 删除博客标签
        /// <summary>
        /// 删除博客文章
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string bloglabledel()
        {
            Int32 BlogLableBh = Request.Form["BlogLableBh"] == null ? 0 : Convert.ToInt32(Request.Form["BlogLableBh"]);
            Int32 intResult = BlogLableBLL.Delete(new Blog_MODEL.BlogLable { BlogLableBh = BlogLableBh });
            string jsonData = null;
            if (intResult > 0)
                jsonData = JsonHelper.ResponseData(StateCode.success, "删除成功", "[]");
            else
                jsonData = JsonHelper.ResponseData(StateCode.failure, "删除失败", "[]");
            return jsonData;
        }
        #endregion
        #region 获取博客标签最大排序值
        /// <summary>
        /// 获取博客文章最大排序值
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Int32 bolglablesortmax()
        {
            return BlogLableBLL.getBlogLalbeSortMax();
        }
        #endregion
        #region 添加修改博客文章
        /// <summary>
        /// 添加修改博客标签
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string addbloglable()
        {
            //添加修改表示(0添加1修改)
            int state = Request.Form["state"] == null ? 0 : Convert.ToInt32(Request.Form["state"]);
            //博客标签名称
            string BlogLableName = Request.Form["BlogLableName"] == null ? "" : Request.Form["BlogLableName"];
            //博客标签编号
            string BlogLableBh = string.IsNullOrEmpty(Request.Form["BlogLableBh"]) ? "0" : Request.Form["BlogLableBh"];
            //博客排序值
            string BlogLableSortValue = Request.Form["BlogLableSortValue"] == null ? "" : Request.Form["BlogLableSortValue"];
            //备注
            string BlogLableNote = Request.Form["BlogLableNote"] == null ? "" : Request.Form["BlogLableNote"];
            int intResult = 0;
            if (state == 0)
            {
                List<Blog_MODEL.BlogLable> listBlogLable = Blog_BLL.BlogLableBLL.QueryList(new Blog_MODEL.BlogLable { BlogLableName = BlogLableName.ToUpper() });
                if (listBlogLable.Count > 0)
                    return JsonHelper.ResponseData(StateCode.failure, "操作失败,已经有此标题的标签", "[]");
            }
            BlogLable _BlogLable = new BlogLable
            {
                BlogLableBh = Convert.ToInt32(BlogLableBh),
                BlogLableName = BlogLableName,
                BlogLableSortValue = Convert.ToInt32(BlogLableSortValue),
                BlogLableNote = BlogLableNote
            };
         
            if (state == 0)//添加
            {
                intResult = BlogLableBLL.Insert(_BlogLable);
            }
            else
            {//修改
                intResult = BlogLableBLL.Update(_BlogLable);
            }
            string jsonData = null;
            if (intResult > 0)
                jsonData = JsonHelper.ResponseData(StateCode.success, "操作成功", "[]");
            else
                jsonData = JsonHelper.ResponseData(StateCode.failure, "操作失败", "[]");
            return jsonData;
        }
        #endregion
    }
}