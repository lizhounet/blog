﻿using System.Web.Mvc;

namespace Blog_UI.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Admin_default",
                url: "Admin/{controller}/{action}/{id}/{*catchall}",
                defaults: new { controller = "Home", action = "Backstage_home", id = UrlParameter.Optional },
                namespaces: new string[] { "Blog_UI.Areas.Admin.Controllers" }
            );
        }
    }
}