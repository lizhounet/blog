﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog_UI.Controllers
{
    public class DefaultController : Controller
    {
        /// <summary>
        /// 验证用户是否登录
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            Blog_BLL.JsonMessages jsonMeg = Blog_BLL.UserLoginBLL.CheckLogin();
            if (jsonMeg.StateCode == Blog_BLL.StateCode.failure)
            {
                //重定向至登录页面
                filterContext.Result = Content("<script>alert('" + jsonMeg.Messages + "'); top.location.href = '/Admin/Login/Index';</script>");
            }

        }
    }
}