﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog_UI.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Default1
        public ActionResult Index(int id=404)
        {
            Response.StatusCode = id;
            return View();
        }
    }
}